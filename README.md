# yancpcRaspi

Qt5 GUI for Raspberri Pi, or use with touchscreens on embedded systems.

Currently can play mp3 format music from a folder and nothing more.

## Setting up toolchain & Compiling

TODO: Research new ways, such as (https://doc.qt.io/QtForDeviceCreation/qtb2-index.html), although having full raspbian (lite) might be beneficial....

Use the instructions from here: http://wiki.qt.io/RRaraspberryPi2EGLFS

https://github.com/abhiTronix/raspberry-pi-cross-compilers

Build with QtCreator. 

Requires `omxplayer` on the Raspberry Pi, because qmultimedia did not work. (TODO: Try to get it working)

also you need to install xorg, if using raspbian-lite image

### DEBUG:

raspi toolchain doesnt have gdb with python support -> debugger fails

1. install gdb-multiarch on dev pc -> configure to use that
2. install gdbserver on raspi

## QT CREATOR:

If you add elements in desing, they wont be regognized in code...  
Solution: disable "Shadow Build" in "Projects" mode.

## QT stuff:

physical display properties, to calc dpi, qt will warn of these

```
export QT_QPA_EGLFS_PHYSICAL_WIDTH=154
export QT_QPA_EGLFS_PHYSICAL_HEIGHT=86
```