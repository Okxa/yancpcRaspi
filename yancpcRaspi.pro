#-------------------------------------------------
#
# Project created by QtCreator 2018-10-04T18:50:37
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = yancpcRaspi
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14

DESTDIR=bin #Target file directory
OBJECTS_DIR=generated_files #Intermediate object files directory
MOC_DIR=generated_files #Intermediate moc files directory

SOURCES += \
    src/main.cpp \
    src/qtgpio.cpp \
    src/widget.cpp \
    src/mediaLibrary/Song.cpp \
    src/threads/InputThreadWorker.cpp \
    src/mediaPlayer/MediaPlayerController.cpp \
    src/mediaLibrary/MediaLibrary.cpp \
    src/mediaLibrary/SqliteManager.cpp \
    src/threads/SqliteThreadWorker.cpp \
    src/threads/InputThreadController.cpp \
    src/threads/SqliteThreadController.cpp \
    src/PlaylistViewModel.cpp

HEADERS += \
    src/qtgpio.h \
    src/widget.h \
    src/mediaLibrary/Song.h \
    src/threads/InputThreadWorker.h \
    src/mediaPlayer/MediaPlayerController.h \
    src/mediaLibrary/MediaLibrary.h \
    src/mediaLibrary/SqliteManager.h \
    src/threads/SqliteThreadWorker.h \
    src/threads/InputThreadController.h \
    src/threads/SqliteThreadController.h \
    src/PlaylistViewModel.h

FORMS += \
    forms/widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    res/gpio.json

RESOURCES += \
    res/res.qrc


unix:!macx: LIBS += -L$$PWD/../raspicarpc/sysroot/usr/lib/arm-linux-gnueabihf/ -ltag

INCLUDEPATH += $$PWD/../raspicarpc/sysroot/usr/include/taglib
DEPENDPATH += $$PWD/../raspicarpc/sysroot/usr/include/taglib
