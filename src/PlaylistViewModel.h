#ifndef PLAYLISTVIEWMODEL_H
#define PLAYLISTVIEWMODEL_H

#include <QObject>
#include <QImage>
#include <QAbstractListModel>
#include "src/mediaLibrary/Song.h"
/*!
 * \brief The PlaylistViewModel class is an model representing a one-dimensional playlist with multiple songs.
 */
class PlaylistViewModel : public QAbstractListModel
{
    Q_OBJECT
public:
    PlaylistViewModel(QObject *parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index = QModelIndex(), int role = Qt::DisplayRole) const override;
    QList<Song> playlist;
public slots:
    void startPlaying(const QModelIndex &index);
signals:
    /*!
     * \brief Emitted to send a signal to MediaPlayerController to start playing a song
     * \param filepath Path to the music file
     */
    void playTrack(QString filepath);
    /*!
     * \brief Emitted to update ui in Widget::updateSongInfoUI
     * \param title Title of the Song
     * \param album Album of the Song
     * \param artist Artist of the Song
     * \param cover Cover of the Song
     * \param isReset If true, clears the displayed info.
     */
    void info(QString title, QString album, QString artist, QImage cover, bool isReset);
private:
    Song currentSong;
    int currentSongIndex;
    void play(Song song);
};

#endif // PLAYLISTVIEWMODEL_H
