#include "MediaPlayerController.h"
/*!
 * \brief Constructs MediaPlayerController, which controls external media player process
 * \param mediaPlayerProcess The process name. Only "omxplayer" supported for now.
 */
MediaPlayerController::MediaPlayerController(QString mediaPlayerProcess)
{
    // set the used process
    processName = mediaPlayerProcess;
    player = new QProcess(this);
}
/*!
 * \brief Changes track in media player process.
 * \param songPath Path to the media file.
 */
void MediaPlayerController::switchTrack(QString songPath)
{
    stop();
    arguments.clear();
    arguments.append(songPath);
    player->start(processName, arguments);
    playerState = playerStates::Playing;
    emit returnResult(playerState);
}
/*!
 * \brief Toggless the pause or play state of the player by writing to the processes STDIN
 */
void MediaPlayerController::playPause()
{
    if (playerState == playerStates::Playing) {
        player->write("p");
        playerState = playerStates::Paused;
    }
    else if (playerState == playerStates::Paused) {
        player->write("p");
        playerState = playerStates::Playing;
    }
    else if (playerState == playerStates::Stopped) {
        switchTrack(currentTrack);
    }
    emit returnResult(playerState);
}
/*!
 * \brief Ends the process by writing to the processes STDIN
 */
void MediaPlayerController::stop()
{
    // check if the process is running
    if (player->state() != QProcess::ProcessState::NotRunning) {
        player->write("q");
        player->waitForFinished();
        playerState = playerStates::Stopped;
    }
    emit returnResult(playerState);
}
