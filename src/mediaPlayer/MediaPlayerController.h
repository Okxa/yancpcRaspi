#ifndef MEDIATHREADWORKER_H
#define MEDIATHREADWORKER_H

#include <QObject>
#include <QProcess>
#include <QMetaEnum>
/*!
 * \brief The MediaPlayerController class is used for controlling external media player process
 */
class MediaPlayerController : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Describes the state of the player
     */
    enum playerStates { Playing, Paused, Stopped, Error_noPathToFile, Error_noValidCommand };
    Q_ENUM(playerStates)
    MediaPlayerController(QString mediaPlayerProcess);
public slots:
    void switchTrack(QString songPath);
    void playPause();
    void stop();
signals:
    /*!
     * \brief Emits the player state, to use in Widget
     * \param state MediaPlayerController::playerStates
     */
    void returnResult(playerStates state);

private:
    // last track
    QString currentTrack;
    // this is used for getting info about enum, such as enum name
    QMetaEnum metaEnumPlayerState = QMetaEnum::fromType<MediaPlayerController::playerStates>();
    // current enum value
    playerStates playerState = playerStates::Stopped;
    // player process
    QProcess *player;
    // which process to use for player
    QString processName;
    // command line arguments for player
    QStringList arguments;
};

#endif // MEDIATHREADWORKER_H
