#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QScroller>
#include "src/PlaylistViewModel.h"
#include "mediaPlayer/MediaPlayerController.h"

namespace Ui {
class Widget;
}
/*!
 * \brief The Widget class is the "main window" of this application. It has all the ui elements and their needed signals
 */
class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void receiveMediaLibResult(QString result);
    void receiveMediaLibSongs(QList<Song> songlist);
    void updatePlayerStatus(MediaPlayerController::playerStates state);
signals:
    /*!
     * \brief Is Emitted to pass path to a file to mediaLibrary
     * \param filePath Path to the music file
     */
    void changeTrack(QString filePath);
    /*!
     * \brief Starts a music scan in folders, when connected to mediaLibrary
     * \param mediafolders
     */
    void startMusicScan(QList<QString> mediafolders);
    /*!
     * \brief Toggless Play and Pause in MediaPlayerController, when connected
     */
    void togglePlayPause();
    /*!
     * \brief Stops player in MediaPlayerController, when connected
     */
    void stopPlayer();
private slots:
    void updateSongInfoUI(QString title, QString album, QString artist, QImage cover, bool isReset);
    void updatePlaylistIndex(const QModelIndex &index);
    void on_btnPlayPause_released();
    void on_btnStop_released();
    void on_btnNext_released();
    void on_btnPrev_released();
    void on_quit_released();
    void on_musicPlayer_released();
    void on_settings_released();
    void on_btnScanMusic_released();
private:
    Ui::Widget *ui;
    QString resultString;
    QModelIndex modelIndex;
    PlaylistViewModel playlistmodel;
    MediaPlayerController::playerStates playerState;
};

#endif // WIDGET_H
