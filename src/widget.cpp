#include "widget.h"
#include "ui_widget.h"
/*!
 * \brief Constructs Widget
 * \param parent Qt default
 */
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    //playlist viewmodel
    playlistmodel(nullptr),
    playerState(MediaPlayerController::playerStates::Stopped)
{
    ui->setupUi(this);
    // playlist touch enabled
    QScroller::grabGesture(ui->playlistListView, QScroller::LeftMouseButtonGesture);
    // playlist connections
    connect(ui->playlistListView, &QListView::doubleClicked, &playlistmodel, &PlaylistViewModel::startPlaying);
    connect(&playlistmodel, &PlaylistViewModel::playTrack, this, &Widget::changeTrack);
    connect(&playlistmodel, &PlaylistViewModel::info, this, &Widget::updateSongInfoUI);
}
/*!
 * \brief Destructs Widget
 */
Widget::~Widget()
{
    delete ui;
}
/*!
 * \brief Slot to receive mediaLibrary scan result updates. Displays the result in QLabel
 * \param result the result to receive
 */
void Widget::receiveMediaLibResult(QString result) {
    ui->lblScanStatus->setText(result);
}
/*!
 * \brief Slot to receive songlist from mediaLibrary scan. Puts the songlist to the PlaylistViewModel and connects this model to a view.
 * Also Connects view to the model so that the model starts playing the selected song via PlaylistViewModel::startPlaying
 * \param songlist
 */
void Widget::receiveMediaLibSongs(QList<Song> songlist)
{
    // set the playlist
    playlistmodel.playlist = songlist;
    // create a index
    modelIndex = playlistmodel.index(0, 0);
    //set the model to the view
    ui->playlistListView->setModel( &playlistmodel );
    // set the view index to the index created above
    ui->playlistListView->setCurrentIndex(modelIndex);
    // when selection is changed, start playing
    connect(ui->playlistListView->selectionModel(), &QItemSelectionModel::currentChanged, &playlistmodel, &PlaylistViewModel::startPlaying);
}
/*!
 * \brief Slot to receive player state from MediaPlayerController
 * \param state enum indicates the mediaplayers' state
 */
void Widget::updatePlayerStatus(MediaPlayerController::playerStates state)
{
    playerState = state;
    if (playerState == MediaPlayerController::playerStates::Playing) {
        ui->btnPlayPause->setText("▶");
    }
    else if (playerState == MediaPlayerController::playerStates::Paused) {
        ui->btnPlayPause->setText("▌▌");
    }
    else if (playerState == MediaPlayerController::playerStates::Stopped) {
        ui->btnPlayPause->setText("▶");
    }
}
/*!
 * \brief Changes the song info in the GUI
 * \param title Title of the Song
 * \param album Album of the Song
 * \param artist Artist of the Song
 * \param cover Cover of the Song
 * \param isReset If true, clears the displayed info.
 */
void Widget::updateSongInfoUI(QString title, QString album, QString artist, QImage cover, bool isReset)
{
    QImage coverImage ;
    if (isReset == false) {
        ui->lblSongTitle->setText(title);
        ui->lblSongAlbum->setText(album);
        ui->lblSongArtist->setText(artist);
        if (!cover.isNull()) {
            coverImage = cover;
        }
        else {
            // use a placeholder if its null
            coverImage = QImage(":/res/placeholder.png");
        }
        ui->albumArt->setPixmap(QPixmap::fromImage(coverImage).scaled(ui->albumArt->height(), ui->albumArt->height(), Qt::KeepAspectRatio));
    }
    else {
        ui->lblSongTitle->setText("Nothing Playing...");
        ui->lblSongAlbum->clear();
        ui->lblSongArtist->clear();
        ui->albumArt->clear();
    }
}
/*!
 * \brief Slot to change the current QModelIndex. This index is used in the Widget::on_btnNext_released() and Widget::on_btnPrev_released() -functions.
 * \param index
 */
void Widget::updatePlaylistIndex(const QModelIndex &index)
{
    modelIndex = index;
}
/*!
 * \brief Handles the play/pause button
 */
void Widget::on_btnPlayPause_released()
{
    if (playerState == MediaPlayerController::playerStates::Playing) {
        emit togglePlayPause();
    }
    else if (playerState == MediaPlayerController::playerStates::Paused) {
        emit togglePlayPause();
    }
    else if (playerState == MediaPlayerController::playerStates::Stopped) {
        // start at current index if stopped
        playlistmodel.startPlaying(ui->playlistListView->currentIndex());
    }
}
/*!
 * \brief Handles the stop button
 * Emits stopPlayer()
 */
void Widget::on_btnStop_released()
{
    emit stopPlayer();
    updateSongInfoUI("","","", QImage::fromData(nullptr), true);
}
/*!
 * \brief Handles the next track -button. Clears the selection in the view, and selects the next one.
 */
void Widget::on_btnNext_released()
{
    ui->playlistListView->clearSelection();
    modelIndex = ui->playlistListView->model()->index(modelIndex.row() + 1, 0);
    ui->playlistListView->setCurrentIndex(modelIndex);
}

/*!
 * \brief Handles the previous track -button. Clears the selection in the view, and selects the previous one.
 */
void Widget::on_btnPrev_released()
{
    ui->playlistListView->clearSelection();
    modelIndex = ui->playlistListView->model()->index(modelIndex.row() - 1, 0);
    ui->playlistListView->setCurrentIndex(modelIndex);
}
void Widget::on_quit_released()
{
    QCoreApplication::quit();
}
void Widget::on_musicPlayer_released()
{
    ui->mainPanel->setCurrentIndex(0);
}
void Widget::on_settings_released()
{
    ui->mainPanel->setCurrentIndex(1);
}
/*!
 * \brief Handles the button to scan for music
 * Emits startMusicScan();
 */
void Widget::on_btnScanMusic_released()
{
    QList<QString> dirs;
    dirs.append("/home/pi/testMusic");
    emit startMusicScan(dirs);
}
