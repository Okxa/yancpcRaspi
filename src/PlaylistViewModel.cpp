#include "PlaylistViewModel.h"
/*!
 * \brief Constructs PlaylistViewModel and required helpers
 * \param parent
 */
PlaylistViewModel::PlaylistViewModel(QObject *parent) :
    QAbstractListModel (parent),
    currentSong(nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr)
{
    currentSongIndex = 0;
}
/*!
 * \brief Reimplements QAbstractListModel::rowCount. Returns the number of rows under the given parent. When the parent is valid it means that rowCount is returning the number of children of parent.
 * \param parent
 * \return Returns rowcount as the length of the playlist data. Then the QListView shows all the Song -objects in the playlist
 */
int PlaylistViewModel::rowCount(const QModelIndex &parent) const
{
    return playlist.length();
}
/*!
 * \brief  Reimplements QAbstractItemModel::data. Returns the data stored under the given role for the item referred to by the index.
 * \param index is the QModelIndex of the selected item in the QListView
 * \param role is a enum Qt::ItemDataRole, controls which data is returned to the QListView, depending on the context
 * \return Returns the name of the Song object in the playlist location corresponding to the \a index;
 */
QVariant PlaylistViewModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole) {
        // bind per row from table
        return playlist[index.row()].path.split("/").last();
    }
    return QVariant();
}
/*!
 * \brief Selects a song corresponding to \a index; from the playlist, and plays it with Play()
 * \param index is the QModelIndex of the selected item in the QListView
 */
void PlaylistViewModel::startPlaying(const QModelIndex &index)
{
    currentSong = playlist[index.row()];
    currentSongIndex = index.row();
    play(currentSong);
}
/*!
 * \brief Emits playTrack() and info().
 * \param song Song -object which is to be played
 */
void PlaylistViewModel::play(Song song)
{
    emit playTrack(song.path);
    emit info(song.title, song.album, song.artist, QImage::fromData(song.cover), false);
}
