#ifndef MEDIALIBRARY_H
#define MEDIALIBRARY_H

#include <QObject>
#include <QStandardPaths>
#include "src/mediaLibrary/Song.h"
#include "src/threads/SqliteThreadController.h"
/*!
 * \brief The MediaLibrary class handles music library in this application.
 */
class MediaLibrary : public QObject
{
    Q_OBJECT
public:
    MediaLibrary();
signals:
    /*!
     * \brief Starts a music scan in folders, when connected to SqliteThreadController
     * \param mediafolders
     */
    void scanSongs(QList<QString> mediafolders);
    /*!
     * \brief Helper. When connected to SqliteThreadController (and which is connected to SqliteThreadWorker), gets songs from database
     */
    void getSongs();
    /*!
     * \brief Helper to get scan status from SqliteThreadController and SqliteThreadWorker
     * \param result
     */
    void sendResult(QString result);
    /*!
     * \brief Helper to get songs from SqliteThreadController and SqliteThreadWorker
     * \param result
     */
    void sendSongs(QList<Song> songlist);
private:
    SqliteThreadController qsqliteThreadController;
};

#endif // MEDIALIBRARY_H
