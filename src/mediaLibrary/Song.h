#ifndef SONG_H
#define SONG_H

#include <QObject>
/*!
 * \brief The Song class is a representation of the most important metadata tags found in music files.
 */
class Song
{
public:
    Song(QString path, QString title, QString artist, QString album, QString track, QString discnumber, QString year, QByteArray cover);
    /*!
     * \brief Path to the file
     */
    QString path;
    /*!
     * \brief Title of the Song
     */
    QString title;
    /*!
     * \brief Artist of the Song
     */
    QString artist;
    /*!
     * \brief Album of the Song
     */
    QString album;
    /*!
     * \brief Track Number of the Song
     */
    QString track;
    /*!
     * \brief Disc Number of the Song
     */
    QString discnumber;
    /*!
     * \brief  Release Year of the Song
     */
    QString year;
    /*!
     * \brief Cover of the Song, in QByteArray form.
     */
    QByteArray cover;
};

#endif // SONG_H
