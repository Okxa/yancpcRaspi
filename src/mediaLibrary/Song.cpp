#include "Song.h"
/*!
 * \brief Constructs Song -object
 * \param input_path Path to the file
 * \param input_title Title of the Song
 * \param input_artist Artist of the Song
 * \param input_album Album of the Song
 * \param input_track Track Number of the Song
 * \param input_discnumber Disc Number of the Song
 * \param input_year Release Year of the Song
 * \param input_cover Cover of the Song, in QByteArray form.
 */
Song::Song(QString input_path, QString input_title, QString input_artist, QString input_album, QString input_track, QString input_discnumber, QString input_year, QByteArray input_cover)
{
    path = input_path;
    title = input_title;
    artist = input_artist;
    album = input_album;
    track = input_track;
    discnumber = input_discnumber;
    year = input_year;
    cover = input_cover;
}
