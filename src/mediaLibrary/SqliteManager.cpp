#include "SqliteManager.h"
#include <QtDebug>
/*!
 * \brief Constructs SqliteManager and connects to a database, checks for a table "songs" and if it doesnt exist; creates it.
 * \param database path to the database
 */
SqliteManager::SqliteManager(const QString database)
{
    //debug to console
    connect(this, &SqliteManager::result, this, &SqliteManager::debuginfo);
    // set database to qsqlite driver
    musicDatabase = QSqlDatabase::addDatabase("QSQLITE");
    // set which databse file to suse
    musicDatabase.setDatabaseName(database);
    // if opening was successfull
    if (musicDatabase.open()) {
        emit result("Successfully opened: " + database);
        // check that table exists
        if (!musicDatabase.tables().contains("songs")) {
            // if not create it
            QSqlQuery query;
            query.prepare("CREATE TABLE IF NOT EXISTS songs "
                          "(path TEXT NOT NULL, title TEXT, artist TEXT, album TEXT, track TEXT, discnumber TEXT, year TEXT, cover BLOB, PRIMARY KEY(path))");
            if (query.exec()) {
                emit result("Database " + database + " created table 'songs'");
            }
            else {
                emit result("Database " + database + " creating table 'songs' failed. Reason: " + query.lastError().text());
            }
        }
        else {
            emit result("Database " + database + " has table 'songs'");
        }
    }
    else {
        emit result("Failed Opening: " + database + " Error: " + musicDatabase.lastError().text());
    }
}
/*!
 * \brief Adds a single Song -object to the database
 * \param song The song which will be added
 * \param index The index of the song. This is only used on output, to see how many songs were added.
 */
void SqliteManager::addSong(Song song, int index)
{
    // create query
    QSqlQuery query;
    query.prepare("INSERT OR REPLACE INTO songs (path, title, artist, album, track, discnumber, year, cover) "
                  "VALUES (:path, :title, :artist, :album, :track, :discnumber, :year, :cover)");
    query.bindValue(":path", song.path);
    query.bindValue(":title", song.title);
    query.bindValue(":artist", song.artist);
    query.bindValue(":album", song.album);
    query.bindValue(":track", song.track);
    query.bindValue(":discnumber", song.discnumber);
    query.bindValue(":year", song.year);
    query.bindValue(":cover", song.cover);
    // exec and if success
    if (query.exec()) {
        // emit result
        emit result("Total found: " + QString::number(index) + " Added song: " + song.path + " to database");
    }
    else {
        emit result("Failed to add song: " + song.path + " to database. Error: " + query.lastError().text());
    }
}
/*!
 * \brief Gets all songs from the database
 */
void SqliteManager::getSongs()
{
    QSqlQuery query;
    query.prepare("SELECT * FROM songs");
    if (query.exec()) {
        int path = query.record().indexOf("path");
        int title = query.record().indexOf("title");
        int artist = query.record().indexOf("artist");
        int album = query.record().indexOf("album");
        int track = query.record().indexOf("track");
        int discnumber = query.record().indexOf("discnumber");
        int year = query.record().indexOf("year");
        int cover = query.record().indexOf("cover");
        while (query.next()) {
            // create a new song object
            Song song(query.value(path).toString(), query.value(title).toString(), query.value(artist).toString(), query.value(album).toString(), query.value(track).toString(), query.value(discnumber).toString(), query.value(year).toString(), query.value(cover).toByteArray());

            songs.append(song);
        }
        // emit songs
        emit sendSongs(songs);
    }
}
/*!
 * \brief Writes to qDebug()
 * \param msg The message to write.
 */
void SqliteManager::debuginfo(QString msg)
{
    qDebug() << msg;
}
