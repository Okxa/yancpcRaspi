#ifndef SQLITEMANAGER_H
#define SQLITEMANAGER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

#include "src/mediaLibrary/Song.h"
/*!
 * \brief The SqliteManager class is used for interfacing with sqlite3 databases
 */
class SqliteManager : public QObject
{
    Q_OBJECT
public:
    SqliteManager(const QString database);
public slots:
    void addSong(Song song, int index);
    void getSongs();
signals:
    /*!
     * \brief Sends the songlist to SqliteThreadWorker (to eventually show in GUI etc)
     * \param songlist
     */
    void sendSongs(QList<Song> songlist);
    /*!
     * \brief Sends the scan status to SqliteThreadWorker (to eventually show in GUI etc)
     * \param response The message to relay
     */
    void result(QString response);
private slots:
    void debuginfo(QString msg);
private:
    QSqlDatabase musicDatabase;
    QList<Song> songs;
};

#endif // SQLITEMANAGER_H
