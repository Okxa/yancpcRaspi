#include "MediaLibrary.h"
#include <QtDebug>
/*!
 * \brief Constructs MediaLibrary -object, which constructs a SqliteThreadController and connects the signals between them
 */
MediaLibrary::MediaLibrary() :
    qsqliteThreadController(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/" + "musicDatabase.sqlite3")
{
    connect(this, &MediaLibrary::scanSongs, &qsqliteThreadController, &SqliteThreadController::scanSongs);
    connect(&qsqliteThreadController, &SqliteThreadController::sendSongs, this, &MediaLibrary::sendSongs);
    connect(&qsqliteThreadController, &SqliteThreadController::sendResult, this, &MediaLibrary::sendResult);
    connect(this, &MediaLibrary::getSongs, &qsqliteThreadController, &SqliteThreadController::getSongs);
}
