#include "widget.h"
#include <QApplication>

#include "src/threads/InputThreadController.h"
#include "src/mediaPlayer/MediaPlayerController.h"
#include "src/mediaLibrary/MediaLibrary.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    // thread controller fro GPIO input
    InputThreadController inputThreadController;
    // connect the thread

    // connect the about to quit signal to stop the thread before exiting application.
    QObject::connect(qApp, &QApplication::aboutToQuit, &inputThreadController, &InputThreadController::stopThread);

    // media player controller
    MediaPlayerController omxplayerController("omxplayer");
    // connect from ui to controller
    QObject::connect(&w, &Widget::changeTrack, &omxplayerController, &MediaPlayerController::switchTrack);
    QObject::connect(&w, &Widget::togglePlayPause, &omxplayerController, &MediaPlayerController::playPause);
    QObject::connect(&w, &Widget::stopPlayer, &omxplayerController, &MediaPlayerController::stop);
    // connect ui updating signals
    QObject::connect(&omxplayerController, &MediaPlayerController::returnResult, &w, &Widget::updatePlayerStatus);
    // connect so that the player process is ended before exiting qt app
    QObject::connect(qApp, &QApplication::aboutToQuit, &omxplayerController, &MediaPlayerController::stop);


    // connect inputhread to mediaplayer
    /* TODO
    QObject::connect(&w, &Widget::on_btnNext_released, &omxplayerController, &mediaPlayerController::nextTrack);
    QObject::connect(&w, &Widget::on_btnPrev_released, &omxplayerController, &mediaPlayerController::prevTrack);
    QObject::connect(&w, &Widget::on_btnPlayPause_released, &omxplayerController, &mediaPlayerController::playPause);
    QObject::connect(&w, &Widget::on_btnStop_released, &omxplayerController, &mediaPlayerController::stop);
    */
    //init media lib
    MediaLibrary musicLib;
    // connect music lib
    // 1. scanning
    QObject::connect(&w, &Widget::startMusicScan, &musicLib, &MediaLibrary::scanSongs);
    // scan results
    qRegisterMetaType< QList<QString> >( "QList<QString>" );
    qRegisterMetaType< QList<Song> >( "QList<Song>" );
    QObject::connect(&musicLib, &MediaLibrary::sendResult, &w, &Widget::receiveMediaLibResult);
    QObject::connect(&musicLib, &MediaLibrary::sendSongs, &w, &Widget::receiveMediaLibSongs);
    musicLib.getSongs();

    // show gui
    w.show();
    return a.exec();
}
