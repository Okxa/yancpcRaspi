#ifndef INPUTTHREADCONTROLLER_H
#define INPUTTHREADCONTROLLER_H

#include <QObject>
#include <QThread>

#include "src/qtgpio.h"
#include "InputThreadWorker.h"
/*!
\brief The InputThreadController class is for creating and controlling InputThreadWorker -class.
*/
class InputThreadController : public QObject
{
    Q_OBJECT
    QThread workerThread;
public:
    InputThreadController();
    ~InputThreadController();
signals:
    /*!
     * \brief Starts the thread
     */
    void startThread();
    /*!
     * \brief Stops the thread, is connected to QThread::requestInterrupt();
     */
    void stopThread();
    void sendResult(QString result);
};

#endif // INPUTTHREADCONTROLLER_H
