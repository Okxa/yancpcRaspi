#ifndef INPUTTHREADWORKER_H
#define INPUTTHREADWORKER_H

#include <QCoreApplication>
#include <QObject>
#include <QThread>
#include "src/qtgpio.h"
/*!
\brief The InputThreadWorker class is for running GPIO inputchecks in separate thread. It is not used by itself, but via InputThreadController -class
*/
class InputThreadWorker : public QObject
{
    Q_OBJECT
public:
    InputThreadWorker();
    ~InputThreadWorker();
public slots:
    void doWork();
signals:
    void returnResult(QString result);

private:
    IoController GPIO;
    bool currentValue;
    QList<bool> values;
};
#endif // INPUTTHREADWORKER_H
