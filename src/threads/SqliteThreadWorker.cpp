#include "SqliteThreadWorker.h"
#include <QtDebug>
/*!
 * \brief Constructs SqliteThreadWorker which creates a SqliteManager and connects it to this SqliteThreadWorker slots
*/
SqliteThreadWorker::SqliteThreadWorker(QString databasePath) :
    musicDB(databasePath)
{
    connect(&musicDB, &SqliteManager::result, this, &SqliteThreadWorker::returnResult);
    connect(this, &SqliteThreadWorker::getSongs, &musicDB, &SqliteManager::getSongs);
    connect(&musicDB, &SqliteManager::sendSongs, this, &SqliteThreadWorker::returnSongs);
};
/*!
 * \brief Scans songs in folders with QDirIterator
 * \param mediafolders QList of QString -objects, which are paths to folders to scan.
 */
void SqliteThreadWorker::scanSongs(QList<QString> mediafolders)
{
    QString title, artist, album, track, discnumber, year;
    QByteArray cover;
    QList<Song> songs;
    // loop through folders defined
    foreach (QString folder, mediafolders) {
        qDebug() << "folder: " + QString::number(mediafolders.indexOf(folder)) + folder;
        // in each mediafolder, loop though every folder
        // look for mp3 files
        QDirIterator iterator(folder, QStringList() << "*.mp3", QDir::Files, QDirIterator::Subdirectories);
        // advance iterator by one so the first is not empty result
        iterator.next();
        // loop while dirs remain
        while (iterator.hasNext()) {

            //load metadata here
            QByteArray ba = iterator.filePath().toLocal8Bit();
            TagLib::FileName name(ba.data());
            TagLib::MPEG::File f(name);
            TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
            // read the first saved image frame
            // APIC is id3v2 frame for images
            TagLib::ID3v2::AttachedPictureFrame *coverart = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(tag->frameListMap()["APIC"].front());
            // turn to QByteArray
            QByteArray* coverByteArr = new QByteArray(reinterpret_cast<const char*>(coverart->picture().data()));

            title = iterator.fileName();
            QStringList splitPath = iterator.filePath().split("/");
            if (splitPath.length() > 0) {
                album = iterator.filePath().split("/")[iterator.filePath().split("/").length() - 2];
                artist = iterator.filePath().split("/")[iterator.filePath().split("/").length() - 3];
                Song song(iterator.filePath(), title, artist, album, track, discnumber, year, cover);
                songs.append(song);
            }
            iterator.next();
        }
    }
    addSongs(songs);
};
/*!
 * \brief Adds list of Song objects in database.
 * \param songList, QList<Song>, QList of Song -objects.
 */
void SqliteThreadWorker::addSongs(QList<Song> songList) {
    for (int i = 0; i < songList.length(); i++) {
        musicDB.addSong(songList[i], i);
    }
}
