#ifndef SQLITETHREADCONTROLLER_H
#define SQLITETHREADCONTROLLER_H

#include <QObject>
#include <QThread>

#include "src/mediaLibrary/Song.h"
#include "SqliteThreadWorker.h"

/*!
\brief The SqliteThreadController class is for creating and controlling SqliteThreadWorker -class.
*/
class SqliteThreadController : public QObject
{
    Q_OBJECT
    QThread workerThread;
public:
    SqliteThreadController(QString databasePath);
    ~SqliteThreadController();
signals:
    /*!
     * \brief Connects to SqliteThreadWorker to scan songs in folders with QDirIterator and save them in sqlite database
     * \param mediafolders QList of QString -objects, which are paths to folders to scan.
     */
    void scanSongs(QList<QString> input_mediafolders);
    /*!
     * \brief Connects to SqliteThreadWorker and gets the songs
     */
    void getSongs();
    /*!
     * \brief Relays the geted songs
     * \param result
     */
    void sendSongs(QList<Song> result);
    /*!
     * \brief Relays the file scan status
     * \param result The message to relay.
     */
    void sendResult(QString result);
};

#endif // SQLITETHREADCONTROLLER_H
