#ifndef SQLITETHREADWORKER_H
#define SQLITETHREADWORKER_H

#include <QCoreApplication>
#include <QObject>
#include <QDirIterator>

#include "src/mediaLibrary/Song.h"
#include "src/mediaLibrary/SqliteManager.h"


#include <taglib/mpegfile.h>
#include <taglib/id3v2tag.h>
#include <taglib/attachedpictureframe.h>


/*!
\brief The SqliteThreadWorker class is to scan songs in folders with QDirIterator and save them in sqlite via SqliteManager.
*/
class SqliteThreadWorker : public QObject
{
    Q_OBJECT
public:
    SqliteThreadWorker(QString databasePath);
public slots:
    void scanSongs(QList<QString> input_mediafolders);
signals:
    /*!
     * \brief Connects to SqliteManager and gets the songs from database
     */
    void getSongs();
    /*!
     * \brief Relays the file scan status
     * \param result The message to relay.
     */
    void returnResult(QString result);
    /*!
     * \brief Relays the geted songs
     * \param result
     */
    void returnSongs(QList<Song> result);
private slots:
    void addSongs(QList<Song> songList);
private:
    SqliteManager musicDB;
};

#endif // SQLITETHREADWORKER_H
