#include "SqliteThreadController.h"
/*!
 * \brief Constructs SqliteThreadController which constructs & starts the SqliteThreadWorker
 */
SqliteThreadController::SqliteThreadController(QString databasePath) {
    SqliteThreadWorker *worker = new SqliteThreadWorker(databasePath);
    // put worker to thread
    worker->moveToThread(&workerThread);
    // connect signal so that thread is deleted after finishing
    connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
    // connect operate to workers doWork function
    connect(this, &SqliteThreadController::scanSongs, worker, &SqliteThreadWorker::scanSongs);
    connect(this, &SqliteThreadController::getSongs, worker, &SqliteThreadWorker::getSongs);
    // connect workers returnResult signal to receiveResult slot
    connect(worker, &SqliteThreadWorker::returnResult, this, &SqliteThreadController::sendResult);
    connect(worker, &SqliteThreadWorker::returnSongs, this, &SqliteThreadController::sendSongs);
    workerThread.start();
}
/*!
 * \brief Destroys the SqliteThreadController, and the started SqliteThreadWorker
 */
SqliteThreadController::~SqliteThreadController() {
    workerThread.quit();
    workerThread.wait();
}
