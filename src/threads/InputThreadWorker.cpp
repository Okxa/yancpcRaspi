#include "InputThreadWorker.h"
/*!
 * \brief Constructs InputThreadWorker which exports and sets required GPIO pins
*/
InputThreadWorker::InputThreadWorker() {
    GPIO.exportGPIO(17);
    GPIO.setDirection(17, true);
};
/*!
 * \brief Destroys the InputThreadWorker, unexporting the exported pins.
 */
InputThreadWorker::~InputThreadWorker() {
    GPIO.unexportGPIO(17);
};
/*!
 * \brief Checks the input until the thread receives signal in QThread::requestInterruption() -slot
 */
void InputThreadWorker::doWork() {
    // if no thread interrupt is requested, loop endlessly
    while(!QThread::currentThread()->isInterruptionRequested()) {
        GPIO.value(17, currentValue);
        emit returnResult(QString::number(currentValue));
    }
}
