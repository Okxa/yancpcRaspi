#include "InputThreadController.h"
/*!
 * \brief Constructs InputThreadController which constructs & starts the InputThreadWorker
 */
InputThreadController::InputThreadController() {
    InputThreadWorker *worker = new InputThreadWorker;
    // put worker to thread
    worker->moveToThread(&workerThread);
    // connect signal so that thread is deleted after finishing
    connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
    // connect operate to workers doWork function
    connect(this, &InputThreadController::startThread, worker, &InputThreadWorker::doWork);
    // connect stopthread to workerThreads requestInterrupt slot:
    // this way the loop in the worker can be stopped from outside
    connect(this, &InputThreadController::stopThread, &workerThread, &QThread::requestInterruption);
    // connect workers returnResult signal to receiveResult slot
    connect(worker, &InputThreadWorker::returnResult, this, &InputThreadController::sendResult);
    workerThread.start();
}
/*!
 * \brief Destroys the InputThreadController, and the started InputThreadWorker
 */
InputThreadController::~InputThreadController() {
    workerThread.quit();
    workerThread.wait();
}
